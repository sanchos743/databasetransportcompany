package com.alexandrmezhov.projectdb.controllers;

import com.alexandrmezhov.projectdb.Navigation;
import com.alexandrmezhov.projectdb.ProjectWindows;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class RequestsController {

    public Button back_button_requests_page;
    public TableView table_requests_page;
    public TextField field_surname_name_brigadier;
    public DatePicker field_end_date;
    public DatePicker field_start_date;
    public Button button_request_info_company;
    public Button button_request_transport_routs;
    public Button button_request_drivers;
    public Button button_request_staff;
    public Button button_request_status_transport;
    public Button button_request_number_used_details;
    public Button button_request_number_of_repairs;

    @FXML
    private void initialize () {

        field_end_date.setConverter(getDateConverter("yyyy-MM-dd"));
        field_end_date.setPromptText("Выберите дату");

        field_start_date.setConverter(getDateConverter("yyyy-MM-dd"));
        field_start_date.setPromptText("Выберите дату");

        button_request_number_of_repairs.setOnAction(event -> {
            ArrayList<String> variables = new ArrayList<>();
            LocalDate start_data = field_start_date.getValue();
            LocalDate end_data = field_end_date.getValue();
            if (start_data != null && end_data != null && !start_data.toString().isEmpty() && !end_data.toString().isEmpty()){
                variables.add(start_data.toString());
                variables.add(end_data.toString());
                table_requests_page.getColumns().clear();
                Navigation.fillTable(table_requests_page, "number_of_repairs", variables);
            }
        });

        button_request_staff.setOnAction(actionEvent -> {
            ArrayList<String> variables = new ArrayList<>();
            String field = field_surname_name_brigadier.getText();
            if (field != null && !field.isEmpty()){
                variables.add(field);
                table_requests_page.getColumns().clear();
                Navigation.fillTable(table_requests_page, "staff_hierarchy", variables);
            }
        });

        button_request_number_used_details.setOnAction(actionEvent -> {
                table_requests_page.getColumns().clear();
                Navigation.fillTable(table_requests_page, "number_of_used_details", null);
        });

        button_request_status_transport.setOnAction(actionEvent -> {
                table_requests_page.getColumns().clear();
                Navigation.fillTable(table_requests_page, "status_transport", null);
        });

        button_request_drivers.setOnAction(actionEvent -> {
                table_requests_page.getColumns().clear();
                Navigation.fillTable(table_requests_page, "drivers_and_cars", null);
        });

        button_request_info_company.setOnAction(actionEvent -> {
                table_requests_page.getColumns().clear();
                Navigation.fillTable(table_requests_page, "information_about_company", null);
        });

        button_request_transport_routs.setOnAction(actionEvent -> {
                table_requests_page.getColumns().clear();
                Navigation.fillTable(table_requests_page, "transport_and_routs", null);
        });

        back_button_requests_page.setOnAction(event -> {
            Stage stage = (Stage) back_button_requests_page.getScene().getWindow();
            Navigation.navigateBack(stage);
        });
    }

    private StringConverter<LocalDate> getDateConverter(String dateFormat) {
        // Converter
        StringConverter<LocalDate> converter = new StringConverter<LocalDate>() {
            DateTimeFormatter dateFormatter =
                    DateTimeFormatter.ofPattern(dateFormat);

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        };
        return converter;
    }
}
