package com.alexandrmezhov.projectdb.controllers;

import com.alexandrmezhov.projectdb.Navigation;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class TableChangePersonalController {


    public TextField pass_field;
    public TextField position_person_field;
    public Button back_button_personal_change;
    public TableView table_of_personal;
    public Button add_button_table_of_personal;
    public TextField surname_name_person;
    public Text error_message_text;

    @FXML
    private void initialize () {

        Navigation.fillTable(table_of_personal, "all_staff", null);

        add_button_table_of_personal.setOnAction(event -> {
            ArrayList<String> variables = new ArrayList<>();
            String field1 = surname_name_person.getText();
            String field2 = position_person_field.getText();
            String field3 = pass_field.getText();
            if (field1 != null && field2 != null && field3 != null){
                variables.add(field1);
                variables.add(field2);
                variables.add(field3);
                error_message_text.setText("");
                try {
                    Navigation.addItemInTable("insert_into_staff", variables);
                    table_of_personal.getColumns().clear();
                    Navigation.fillTable(table_of_personal, "all_staff", null);
                } catch (SQLException e) {
                    error_message_text.setText(e.getMessage());
                }
            } else {
                error_message_text.setText("Поля не заполнены!");
            }
        });

        back_button_personal_change.setOnAction(event -> {
            Stage stage = (Stage) back_button_personal_change.getScene().getWindow();
            Navigation.navigateBack(stage);
        });
    }
}
