package com.alexandrmezhov.projectdb.controllers;

import com.alexandrmezhov.projectdb.Navigation;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.sql.SQLException;
import java.util.ArrayList;

public class TableChangeDetailsController {

    public Button back_button_details_change;
    public TextField name_of_detail_field;
    public TableView table_of_details;
    public Button add_button_details;
    public Text error_message_text;

    @FXML
    private void initialize() {
        Navigation.fillTable(table_of_details, "all_details", null);

        add_button_details.setOnAction(event -> {
            ArrayList<String> variables = new ArrayList<>();
            String field1 = name_of_detail_field.getText();
            if (field1 != null && !field1.isEmpty()) {
                variables.add(field1);
                error_message_text.setText("");
                try {
                    Navigation.addItemInTable("insert_into_details", variables);
                    table_of_details.getColumns().clear();
                    Navigation.fillTable(table_of_details, "all_details", null);
                } catch (SQLException e) {
                    error_message_text.setText(e.getMessage());
                }
            } else {
                error_message_text.setText("Поля не заполнены!");
            }
        });

        back_button_details_change.setOnAction(event -> {
            Stage stage = (Stage) back_button_details_change.getScene().getWindow();
            Navigation.navigateBack(stage);
        });
    }
}
