package com.alexandrmezhov.projectdb.controllers;

import com.alexandrmezhov.projectdb.Navigation;
import com.alexandrmezhov.projectdb.ProjectWindows;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class StartPageController {
    @FXML
    private Button requests_button;
    @FXML
    private Button change_tables_button;


    @FXML
    private void initialize () {
        requests_button.setOnAction(event -> {
            Stage stage = (Stage) requests_button.getScene().getWindow();
            Navigation.navigateTo(ProjectWindows.REQEUESTS_PAGE, stage);
        });

        change_tables_button.setOnAction(event -> {
            Stage stage = (Stage) change_tables_button.getScene().getWindow();
            Navigation.navigateTo(ProjectWindows.TABLE_SELECTION_PAGE, stage);
        });
    }
}