package com.alexandrmezhov.projectdb.controllers;

import com.alexandrmezhov.projectdb.Navigation;
import com.alexandrmezhov.projectdb.ProjectWindows;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class TableSelectionController {
    public Button detail_button;
    public Button personal_button;
    public Button back_button_table_selection;


    @FXML
    private void initialize () {
        detail_button.setOnAction(event -> {
            Stage stage = (Stage) detail_button.getScene().getWindow();
            Navigation.navigateTo(ProjectWindows.TABLE_CHANGE_DETAILS_PAGE, stage);
        });

        personal_button.setOnAction(event -> {
            Stage stage = (Stage) personal_button.getScene().getWindow();
            Navigation.navigateTo(ProjectWindows.TABLE_CHANGE_PERSONAL_PAGE, stage);
        });

        back_button_table_selection.setOnAction(event -> {
            Stage stage = (Stage) back_button_table_selection.getScene().getWindow();
            Navigation.navigateBack(stage);
        });
    }
}
