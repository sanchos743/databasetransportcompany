module com.alexandrmezhov.projectdb {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;


    opens com.alexandrmezhov.projectdb to javafx.fxml;
    exports com.alexandrmezhov.projectdb;
    exports com.alexandrmezhov.projectdb.controllers;
    opens com.alexandrmezhov.projectdb.controllers to javafx.fxml;
}