select tt.type_transport, count(*) as number_of_repairs, sum(cost_repair) as total_repair_cost
from transport_repair tr, transport t,  types_transport tt
where tr.id_transport = t.id_transport and t.id_type_transport = tt.id_type_transport and date_repair between '?' and '?'
group by type_transport;