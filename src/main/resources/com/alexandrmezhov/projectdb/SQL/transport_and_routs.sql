select t.transport_brand, tt.type_transport, r.name_route
from transport t
inner join types_transport tt on t.id_type_transport = tt.id_type_transport
inner join routs r on r.id_transport = t.id_transport
where tt.type_transport in ('light car', 'minibus', 'taxi', 'bus');