select t.transport_brand, count(*) as number_of_details
from transport_repair tr, transport t
where tr.id_transport = t.id_transport
group by transport_brand;