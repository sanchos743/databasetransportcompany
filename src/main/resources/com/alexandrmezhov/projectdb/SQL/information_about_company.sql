select t.transport_brand, count(*) as number_of_transport, a.type_attribute, tt.attribute_value, sum(transport_mileage) as total_mileage_of_all_transport
from  transport t, types_transport tt, attributes_transport a
where t.id_type_transport = tt.id_type_transport and tt.id_attribute = a.id_attribute
group by transport_brand;